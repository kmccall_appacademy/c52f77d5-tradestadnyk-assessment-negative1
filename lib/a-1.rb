# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  result = []
  (nums.min..nums.max).each do |ele|
    if !nums.include?(ele)
      result << ele
    end
  end
  result
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  if binary == "10"
    return 2
  end
  result = 0
  each_char = binary.split('')
  i = each_char.length - 1
  j = 0
  while i >= 0
    p result
    p
    result += (each_char[j].to_i * 2 ** i)
    i -= 1
    j += 1
  end
  result
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.
  def my_each
    if self.instance_of?(Array)
      i = 0
      while i < self.size
          yield(self[i])
          i += 1
      end
      self
    elsif self.instance_of?(Hash)
      i = 0
      arr = self.to_a
      while i < arr.size
          yield(arr[i][0], arr[i][1])
          i += 1
      end
      self
    end
  end

  def my_select(&prc)
    ret = {}
      self.my_each do |key, value|
        if prc.call(key,value)
          ret[key] = value
        end
      end
    ret
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    if !block_given?
    hash.each {|key, value| self[key] = value }
    self
  else
  r_hsh = Hash.new {|hsh, key| hsh[key] = {} }
  self.each do |key, val|
    oldval = :oldval
    r_hsh[key][oldval] = val
  end
  hash.each do |key, val|
    newval = :newval
    r_hsh[key][newval] = val
  end
  test_arr = []
  r_hsh.each do |keys, value_arr|
    value_arr.each do |val_type, value|
        if r_hsh[keys][:oldval] == nil
          next
        elsif r_hsh[keys][:newval] == nil
          next
        else
      test_arr << [keys, r_hsh[keys][:oldval], r_hsh[keys][:newval]]
        end
    end
  end
  test_arr.uniq.each do |key, oldv, newv|
      if newv.class == Hash
        r_hsh[key] = prc.call(key, oldv, newv.values[0])
      else
        r_hsh[key] = prc.call(key, oldv, newv)
      end
  end

  return r_hsh

    end
  end


end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas_seq = [2, 1]
  idx = 0
  while idx < (n.abs)-1
    lucas_seq << lucas_seq[idx...idx+2].inject(:+)
    idx += 1
  end
  if n >= 0
    lucas_seq[n]
  elsif n < 0
    if n.abs.odd?
      lucas_seq[n.abs] * -1
    else
      lucas_seq[n.abs]
    end
  end

end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  palindrome_array = []
  string.length.downto(2) do |num|
    palindrome_array << num if string.chars.each_cons(num).find {|str| str == str.reverse}
  end
  return palindrome_array.max || false
end
